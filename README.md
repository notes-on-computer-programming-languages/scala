# scala

https://en.wikipedia.org/wiki/Scala_(programming_language)

[[_TOC_]]

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=rustc%20mono-mcs%20ocaml-base-nox%20ghc%20julia%20scala%20racket%20elixir%20clojure%20chezscheme%20gnat&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)
![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=rustc%20mono-mcs%20ocaml-base-nox%20ghc%20julia%20scala%20racket%20elixir%20clojure%20chezscheme%20gnat&show_vote=on&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)
![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=julia%20scala%20racket%20elixir%20clojure%20chezscheme%20fpc&show_vote=on&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)

# Download
* https://github.com/asdf-community/asdf-scala
* https://github.com/asdf-community/asdf-dotty

# Books
* [Scala (Computer program language)
  ](https://www.worldcat.org/search?q=su%3AScala+%28Computer+program+language%29)
---
* [*Functional Programming in Scala*
  ](https://www.manning.com/books/functional-programming-in-scala-second-edition)
  2023-07 (Second Edition) Michael Pilquist, Rúnar Bjarnason, and Paul Chiusano (Manning)
* [*Functional event-driven architecture: Powered by Scala 3*
  ](https://leanpub.com/feda)
  (2022-12) Gabriel Volpe
* *Scala for the Impatient*
  2022-12 (3rd Edition) Cay S. Horstmann
* *Beginning Scala 3 A Functional and Object-Oriented Java Language*
  2022 (3rd ed.) Vishal Layka (Apress)
* *Beginning Scala 3 A Functional and Object-Oriented Java Language*
  2022 David Pollak
* [*Get programming with Scala*
  ](https://www.worldcat.org/search?q=ti%3AGet+Programming+with+Scala)
  2021-09 Daniela Sfregola
  * Ch. 46 Database queries with Quill
* [*Practical FP in Scala: A hands-on approach*
  ](https://leanpub.com/pfp-scala)
  2021-09 Gabriel Volpe
* [*Scala Cookbook: Recipes for Object-Oriented and Functional Programming*
  ](http://scalacookbook.com)
  2021-08 (2nd Edition) Alvin Alexander
* [*Programming Scala*
  ](https://www.worldcat.org/search?q=ti%3AProgramming+Scala)
  2021-06 (3rd Edition) Dean Wampler
* *Programming in Scala*, Fifth Edition 2021 Martin Odersky, Lex Spoon, Bill Venners,Frank Sommers (Artima, SkillSoft)
  * Updated for Scala3
* *Scala with Cats*
  2020-04 Noel Welsh and Dave Gurnell
  * https://underscore.io/books/scala-with-cats
  * https://www.scalawithcats.com
  * https://www.scalawithcats.com/dist/scala-with-cats.html
* [*SCALA – Functional Programming*](https://doi.org/10.1002/9781119720492.ch15)
  2020-04 Florent Devin (chapter)
* [*Mastering Functional Programming : Functional Techniques for Sequential and Parallel Programming with Scala*
  ](https://www.packtpub.com/product/mastering-functional-programming/9781788620796)
  2018-08 Anatolii Kmetiuk
* [*Scala Reactive Programming*
  ](https://www.packtpub.com/product/scala-reactive-programming/9781787288645)
  2018-02 Rambabu Posa
  * Reactive programming (RP), Function Reactive Programming (FRP), OOP RP, Imperative RP, and Reactive Engine
  * [*Understanding functional reactive programming in Scala [Tutorial]*](https://hub.packtpub.com/understanding-functional-reactive-programming-in-scala/)
* [*Functional Programming: A PragPub Anthology: Exploring Clojure, Elixir, Haskell, Scala, and Swift*
  ](https://pragprog.com/titles/ppanth/functional-programming-a-pragpub-anthology/)
  2017-07 Michael Swaine and the PragPub writers
* [*Functional Programming in Scala*
  ](https://www.manning.com/books/functional-programming-in-scala)
  2014 Paul Chiusano and Runar Bjarnason, Foreword by Martin Odersky
  * 2023-03 edition planed

# Video
* [*Supercharge Scala future : FP-Tower*
  ](https://www.worldcat.org/fr/title/1350411272)
  2022 Julien Truffaut (Packt Publishing)

# Webpages
* [*Scala 3 - and what it means to me*
  ](https://tech.ovoenergy.com/scala-3-and-what-it-means-to-me/)
  2020-01 Matt Sinton-Hewitt
* [*Demystifying the Monad in Scala*
  ](https://medium.com/free-code-camp/demystifying-the-monad-in-scala-cc716bb6f534)
  2015-12 Sinisa Louc

# Online courses
* [Lecture 5.2 - Functional Reactive Programming
  ](https://www.coursera.org/lecture/progfun2/lecture-4-2-functional-reactive-programming-pEsTy)
* https://www.coursera.org/learn/scala-functional-programming

# Tools
* Ammonite
* Mill
* SBT
* [Scalastyle - Scala style checker
  ](http://www.scalastyle.org)
* [Seed](tindzk.github.io/seed/) Build tool for Scala projects

## SBT
### Run a Specific Main Class with Parameters
* [*How to Run a Specific Main Class with Parameters through SBT*
  ](https://blog.ssanj.net/posts/2016-03-02-how-to-run-a-specific-main-class-with-parameters-through-sbt.html)
  2016-03 ssanj.net
* [*18.9. Specifying a Main Class to Run*
  ](https://www.oreilly.com/library/view/scala-cookbook/9781449340292/ch18s10.html)
  * From: Scala Cookbook by Alvin Alexander
  * https://alvinalexander.com/scala/sbt-how-specify-main-method-class-to-run-in-project/

# Libraries
* [*Awesome Scala: A curated list of awesome Scala frameworks, libraries and tools.*
  ](https://index.scala-lang.org/awesome)
  (Scaladex)
* [Libraries
  ](https://www.baeldung.com/scala/category/libraries)
  Baeldung

## Concurrent and parallel programming
* https://tracker.debian.org/pkg/multiverse-core (Java, Scala)
* Akka...

## Configuration
* PureConfig
  * [*Load Configuration Files In Scala Using PureConfig*
    ](https://www.baeldung.com/scala/pureconfig-load-config-files)
    2022-10 Yadu Krishnan

## Databases
* [*Awesome Scala: Databases*
  ](https://index.scala-lang.org/awesome/databases)
  (Scaladex)

### ORM
* [scala orm
  ](https://google.com/search?q=scala+orm)
---
* [*What is a good Scala ORM for a developer who hates boilerplate and loves both FP and databases?*
  ](https://www.quora.com/What-is-a-good-Scala-ORM-for-a-developer-who-hates-boilerplate-and-loves-both-FP-and-databases)
  (2017) (Quora)
  1. ScalikeJdbc Great Sql interop.
  2. Skinny ORM Great ORM over ScalikeJdbc.
  3. SORM Logical Scala ORM.
  4. Slick Functional RM with an async IOMonad approach. Sql interop.
  5. Doobie (tpolecat/doobie) A minimalistic functional wrapper around Jdbc.
* [*Why would I not need an ORM in a functional language like Scala?*
  ](https://softwareengineering.stackexchange.com/questions/303638/why-would-i-not-need-an-orm-in-a-functional-language-like-scala)
  (2017) (Software Engineering)
* [*What ORMs work well with Scala?*
  ](https://stackoverflow.com/questions/1140448/what-orms-work-well-with-scala)
  2012 (Stack Overflow)

## Functional Programming
* [Functional Programming
  ](https://www.baeldung.com/scala/category/functional-programming)
  Baeldung
* Cats
  * [Tag: Cats
    ](https://www.baeldung.com/scala/tag/cats)
    Baeldung
  * [*Scala – Introduction to Cats*
    ](https://www.baeldung.com/scala/cats-intro)
    2022-09 Baeldung
  * [*do vs for*
    ](https://eed3si9n.com/herding-cats/do-vs-for.html)

## Syntax
### Lens
* [*Lens*
  ](https://www.scala-exercises.org/monocle/lens)
  (2023) Scala Exercises
  * https://github.com/scala-exercises/exercises-monocle
* [monocle](https://index.scala-lang.org/optics-dev/monocle)
  Optics library for Scala
  * [*Introduction to Optics in Scala Using Monocle*
    ](https://www.baeldung.com/scala/monocle-optics)
    2023-02 baeldung

# Implementations
## Scala Native
* Scala Native 0.4.3 supports Scala 3.1.\{0..1\}
  * [pdf](https://buildmedia.readthedocs.org/media/pdf/scala-native/latest/scala-native.pdf)
* [*Building Native Applications in Scala Using Scala Native*
  ](https://www.baeldung.com/scala/native-apps-scala-native)
  2022-04 Yadu Krishnan
* [Awesome Scala Native
  ](https://github.com/tindzk/awesome-scala-native)
  (2021-10)
  * [GitHub](https://github.com/tindzk/awesome-scala-native)
* [*Revisiting Scala Native performance*
  ](https://medium.com/virtuslab/revisiting-scala-native-performance-67029089f241)
  2021-04 Wojciech Mazur

### Books
* [*Modern Systems Programming with Scala Native*
  ](https://pragprog.com/titles/rwscala/modern-systems-programming-with-scala-native/)
  2020 Richard Whaling (The Pragmatic Bookshelf)

### Libraries
* [*SNUnit: Scala Native HTTP server based on NGINX Unit*
  ](https://github.com/lolgab/snunit)

# Other languages compared to Scala
## Haskell and Scala
* [*Difference Between Haskell and Scala*
  ](https://www.geeksforgeeks.org/differnece-between-haskell-and-scala/)
  2021-08 miniyadav1
